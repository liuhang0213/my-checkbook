const router = require('express').Router();

router.use('/record', require('./record'));
router.use('/account', require('./account'));
router.use('/category', require('./category'));
router.use('/balance', require('./balance'));

module.exports = router;
