const mongoose = require('mongoose');
const router = require('express').Router();
const Account = mongoose.model('Account');
const Balance = mongoose.model('Balance');

router.post('/', (req, res, next) => {
  const { body } = req;

  if(!body.name) {
    return res.status(422).json({
      errors: {
        name: 'is required',
      },
    });
  }

  if(!body.shortname) {
    return res.status(422).json({
      errors: {
        shortname: 'is required',
      },
    });
  }

  if(!body.startdate) {
    return res.status(422).json({
      errors: {
        startdate: 'is required',
      },
    });
   }

  if(!body.initbalance) {
    return res.status(422).json({
      errors: {
        initbalance: 'is required',
      },
    });
  }

  const finalAccount = new Account(body);

  body['account'] = finalAccount._id;
  body['balance'] = body.initbalance;
  body['date'] = body.startdate;

  const finalBalance = new Balance(body);
  finalBalance.save();

  return finalAccount.save()
    .then(() => res.json({ account: finalAccount.toJSON() }))
    .catch(next);
});

router.get('/', (req, res, next) => {
  return Account.find()
    .sort({ name: 'descending' })
    .then((account) => res.json({ account: account.map(account => account.toJSON()) }))
    .catch(next);
});

router.param('id', (req, res, next, id) => {
  return Account.findById(id, (err, account) => {
    if(err) {
      return res.sendStatus(404);
  } else if(account) {
      req.account = account;
      return next();
    }
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  return res.json({
    account: req.account.toJSON(),
  });
});

router.patch('/:id', (req, res, next) => {
  const { body } = req;

  if(typeof body.name !== 'undefined') {
    req.account.name = body.name;
  }

  if(typeof body.number !== 'undefined') {
    req.account.number = body.number;
  }

  if(typeof body.shortname !== 'undefined') {
    req.account.shortname = body.shortname;
  }

  if(typeof body.desc !== 'undefined') {
    req.account.desc = body.desc;
  }

  if(typeof body.fgColor !== 'undefined') {
    req.account.fgColor = body.fgColor;
  }

  if(typeof body.bgColor !== 'undefined') {
    req.account.bgColor = body.bgColor;
  }

  return req.account.save()
    .then(() => res.json({ account: req.account.toJSON() }))
    .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return Account.findByIdAndRemove(req.account._id)
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
