const mongoose = require('mongoose');
const router = require('express').Router();
const Record = mongoose.model('Record');

router.post('/', (req, res, next) => {
  const { body } = req;

  if(!body.category) {
    return res.status(422).json({
      errors: {
        category: 'is required',
      },
    });
  }

  if(!body.account) {
    return res.status(422).json({
      errors: {
        account: 'is required',
      },
    });
  }

  if(!body.amount) {
    return res.status(422).json({
      errors: {
        amount: 'is required',
      },
    });
  }

  body.confirmed = body.confirmed === 'true' ? true : false;

  const finalRecord = new Record(body);
  return finalRecord.save()
    .then(() => res.json({ record: finalRecord.toJSON() }))
    .catch(next);
});

router.get('/', (req, res, next) => {
  return Record.find()
    .sort({ createdAt: 'descending' })
    .then((record) => res.json({ record: record.map(record => record.toJSON()) }))
    .catch(next);
});

router.param('id', (req, res, next, id) => {
  return Record.findById(id, (err, record) => {
    if(err) {
      return res.sendStatus(404);
  } else if(record) {
      req.record = record;
      return next();
    }
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  return res.json({
    record: req.record.toJSON(),
  });
});

router.patch('/:id', (req, res, next) => {
  const { body } = req;

  if(typeof body.date !== 'undefined') {
    req.record.date = body.date;
  }

  if(typeof body.category !== 'undefined') {
    req.record.category = body.category;
  }

  if(typeof body.account !== 'undefined') {
    req.record.account = body.account;
  }

  if(typeof body.amount !== 'undefined') {
    req.record.amount = body.amount;
  }

  if(typeof body.notes !== 'undefined') {
    req.record.notes = body.notes;
  }

  if(typeof body.attachmentPath !== 'undefined') {
    req.record.attachmentPath = body.attachmentPath;
  }

  if(typeof body.confirmed !== 'undefined') {
    req.record.confirmed = body.confirmed;
  }

  return req.record.save()
    .then(() => res.json({ record: req.record.toJSON() }))
    .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return Record.findByIdAndRemove(req.record._id)
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
