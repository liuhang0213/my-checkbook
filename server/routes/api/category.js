const mongoose = require('mongoose');
const router = require('express').Router();
const Category = mongoose.model('Category');

router.post('/', (req, res, next) => {
  const { body } = req;

  if(!body.name) {
    return res.status(422).json({
      errors: {
        name: 'is required',
      },
    });
  }

  if(!body.fgColor) {
    return res.status(422).json({
      errors: {
        fgColour: 'is required',
      },
    });
  }

  if(!body.type) {
    return res.status(422).json({
      errors: {
        type: 'is required',
      },
    });
  }

  const finalCategory = new Category(body);
  return finalCategory.save()
    .then(() => res.json({ category: finalCategory.toJSON() }))
    .catch(next);
});

router.get('/', (req, res, next) => {
  return Category.find()
    .sort({ name: 'descending' })
    .then((category) => res.json({ category: category.map(category => category.toJSON()) }))
    .catch(next);
});

router.param('id', (req, res, next, id) => {
  return Category.findById(id, (err, category) => {
    if(err) {
      return res.sendStatus(404);
  } else if(category) {
      req.category = category;
      return next();
    }
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  return res.json({
    category: req.category.toJSON(),
  });
});

router.patch('/:id', (req, res, next) => {
  const { body } = req;

  if(typeof body.name !== 'undefined') {
    req.category.name = body.name;
  }

  if(typeof body.fgColor !== 'undefined') {
    req.category.fgColor = body.fgColor;
  }

  if(typeof body.type !== 'undefined') {
    req.category.type = body.type;
  }

  return req.category.save()
    .then(() => res.json({ category: req.category.toJSON() }))
    .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return Category.findByIdAndRemove(req.category._id)
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
