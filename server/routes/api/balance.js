const mongoose = require('mongoose');
const router = require('express').Router();
const Balance = mongoose.model('Balance');


router.post('/', (req, res, next) => {
      return res.sendStatus(403);

/*  const { body } = req;

  if(!body.category) {

  }

  if(!body.account) {
    return res.status(422).json({
      errors: {
        account: 'is required',
      },
    });
  }

  if(!body.amount) {
    return res.status(422).json({
      errors: {
        amount: 'is required',
      },
    });
  }

  body.confirmed = body.confirmed === 'true' ? true : false;

  const finalBalance = new Balance(body);
  return finalBalance.save()
    .then(() => res.json({ balance: finalBalance.toJSON() }))
    .catch(next);
*/
});


router.get('/', (req, res, next) => {
  return Balance.find()
    .sort({ createdAt: 'descending' })
    .then((balance) => res.json({ balance: balance.map(balance => balance.toJSON()) }))
    .catch(next);
});

router.param('id', (req, res, next, id) => {
  return Balance.findById(id, (err, balance) => {
    if(err) {
      return res.sendStatus(404);
  } else if(balance) {
      req.balance = balance;
      return next();
    }
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  return res.json({
    balance: req.balance.toJSON(),
  });
});

router.patch('/:id', (req, res, next) => {
  const { body } = req;

  if(typeof body.amount !== 'undefined') {
    req.body.amount = body.amount;
  }

  return req.body.save()
    .then(() => res.json({ balance: req.balance.toJSON() }))
    .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return Balance.findByIdAndRemove(req.balance._id)
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = router;
