const mongoose = require('mongoose');

const { Schema } = mongoose;

const AccountSchema = new Schema({
  name: { type: String, required: true },
  number: String,
  shortname: { type: String, required: true },
  desc: String,
  fgColor: { type: String, match: /^#[A-Fa-f0-9]{6}$/, default: "#fafafa", required: false },
  bgColor: { type: String, match: /^#[A-Fa-f0-9]{6}$/, default: "#483d8b", required: false },
}, { timestamps: false });

AccountSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    name: this.name,
    number: this.number,
    shortname: this.shortname,
    desc: this.desc,
    fgColor: this.fgColor,
    bgColor: this.bgColor
  };
};

mongoose.model('Account', AccountSchema);
