const mongoose = require('mongoose');
const { Schema } = mongoose;


const RecordSchema = new Schema({
  date: { type: Date, default: Date.now, required: true },
  category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
  account: { type: Schema.Types.ObjectId, ref: 'Account', required: true },
  amount: { type: Number, required: true },
  notes: String,
  attachmentPath: String,
  confirmed: { type: Boolean, default: false },
}, { timestamps: true });

RecordSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    date: this.date,
    category: this.category,
    account: this.account,
    amount: this.amount,
    notes: this.notes,
    attachmentPath: this.attachmentPath,
    confirmed: this.confirmed
  };
};

mongoose.model('Record', RecordSchema);
