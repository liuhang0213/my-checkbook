const mongoose = require('mongoose');

const { Schema } = mongoose;

const CategorySchema = new Schema({
  name: { type: String, required: true },
  fgColor: { type: String, match: /^#[A-Fa-f0-9]{6}$/, default: "#212121", required: true },
  type: { type: String, enum: ['Income', 'Expense'], required: true }
}, { timestamps: true });

CategorySchema.methods.toJSON = function() {
  return {
    _id: this._id,
    name: this.name,
    fgColor: this.fgColor,
    type: this.type
  };
};

mongoose.model('Category', CategorySchema);
