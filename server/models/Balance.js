const mongoose = require('mongoose');
const { Schema } = mongoose;

const BalanceSchema = new Schema({
  date: { type: Date, default: Date.now, required: true },
  account: { type: Schema.Types.ObjectId, ref: 'Account', required: true },
  balance: { type: Number, required: true },
}, { timestamps: true });

BalanceSchema.methods.toJSON = function() {
  return {
    _id: this._id,
    date: this.date,
    account: this.account,
    balance: this.balance,
  };
};

mongoose.model('Balance', BalanceSchema);
