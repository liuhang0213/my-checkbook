import React from 'react';
import axios from 'axios';
import moment from 'moment';
import { connect } from 'react-redux';

import {
    Button,
    Container,
    Header,
    Icon,
    Modal,
    Table
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

import { RecordForm } from '../Record/';
import { Utils } from '../Utils';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = { formOpen: false }

    this.handleDelete = this.handleDelete.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleFormOpen = this.handleFormOpen.bind(this);
    this.handleFormClose = this.handleFormClose.bind(this);
  }

  componentDidMount() {
    const { onLoad } = this.props;

    axios.all([
        axios.get('http://localhost:8000/api/account'),
        axios.get('http://localhost:8000/api/category'),
        axios.get('http://localhost:8000/api/record')
    ]).then(axios.spread(function (acc, cat, rec) {
        let data = acc.data;
        data['category'] = cat.data.category;
        data['record'] = rec.data.record;
        onLoad(data);
    }));
  }

  handleDelete(id) {
    const { onDelete } = this.props;

    return axios.delete(`http://localhost:8000/api/record/${id}`)
      .then(() => onDelete(id));
  }

  handleEdit(record) {
    const { setEdit } = this.props;
    setEdit(record);
    this.handleFormOpen();
  }

  handleFormOpen() {
      this.setState({ formOpen: true })
  }

  handleFormClose() {
      this.setState({ formOpen: false })
  }

  render() {
    const { records, accounts, categories, recordToEdit } = this.props;
    const accountsWithKey = Utils.arrWithIdToObject(accounts);
    const categoriesWithKey = Utils.arrWithIdToObject(categories);

    return (
      <Container>
        <Modal
            trigger={
                <Button icon onClick={this.handleFormOpen}>
                    <Icon name="plus" size="big" color='violet' />
                </Button>}
            open={this.state.formOpen} onClose={this.handleFormClose}>
          <Modal.Header>
            <Icon name='plus' /> Add New Record
          </Modal.Header>
          <Modal.Content>
            <Modal.Description>
                <RecordForm
                    categories={ categories }
                    accounts={ accounts }
                    onClose={ this.handleFormClose }/>
            </Modal.Description>
          </Modal.Content>
        </Modal>
          <p>Records:</p>
          <Table striped>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Date</Table.HeaderCell>
              <Table.HeaderCell>Category</Table.HeaderCell>
              <Table.HeaderCell>Amount</Table.HeaderCell>
              <Table.HeaderCell>Account</Table.HeaderCell>
              <Table.HeaderCell>Notes</Table.HeaderCell>
              <Table.HeaderCell>Confirmed</Table.HeaderCell>
              <Table.HeaderCell>Actions</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
          {records.map((rec) => {
            const cat = categoriesWithKey[rec.category] ? categoriesWithKey[rec.category].name : "--"
            const acc = accountsWithKey[rec.account] ? accountsWithKey[rec.account].name : "--"
            return (
                <Table.Row>
                  <Table.Cell>{ rec.date }</Table.Cell>
                  <Table.Cell>{ cat }</Table.Cell>
                  <Table.Cell>{ rec.amount }</Table.Cell>
                  <Table.Cell>{ acc }</Table.Cell>
                  <Table.Cell>{ rec.notes }</Table.Cell>
                  <Table.Cell>{ rec.confirmed ? "Yes" : "No" }</Table.Cell>
                  <Table.Cell>
                    <Button icon onClick={() => this.handleEdit(rec)}>
                      <Icon name="edit" color='violet' />
                    </Button>
                    <Button icon onClick={() => this.handleDelete(rec._id)}>
                      <Icon name="delete" color='violet' />
                    </Button>
                  </Table.Cell>
                </Table.Row>
            )})}
          </Table.Body>
        </Table>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  records: state.home.records,
  accounts: state.home.accounts,
  categories: state.home.categories,
  recordToEdit: state.home.recordToEdit
});

const mapDispatchToProps = dispatch => ({
  onLoad: data => dispatch({ type: 'HOME_PAGE_LOADED', data }),
  onDelete: id => dispatch({ type: 'DELETE_RECORD', id }),
  setEdit: record => dispatch({ type: 'SET_EDIT', record }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
