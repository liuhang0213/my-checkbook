import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Dropdown,
    Form,
    Input,
    Select,
    TextArea
} from 'semantic-ui-react'

class AccountForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      number: '',
      shortname: '',
      desc: '',
      fgColor: '',
      bgColor: '',
      account: '',
      startdate: '',
      initbalance: 0
    }

    this.handleChangeField = this.handleChangeField.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if(this.props.accountToEdit) {
      this.setState({
        name: this.props.accountToEdit.name,
        number: this.props.accountToEdit.number,
        shortname: this.props.accountToEdit.shortname,
        desc: this.props.accountToEdit.desc,
        fgColor: this.props.accountToEdit.fgColor,
        bgColor: this.props.accountToEdit.bgColor,
      });
    }
  }

  handleSubmit(){
    const { onSubmit, accountToEdit, onEdit } = this.props;
    const { name, number, shortname, desc, fgColor, bgColor, startdate, initbalance } = this.state;

    if(!accountToEdit) {
      return axios.post('http://localhost:8000/api/account', {
        name,
        number,
        shortname,
        desc,
        fgColor,
        bgColor,
        startdate,
        initbalance
      })
        .then((res) => onSubmit(res.data))
        .then(() => this.props.onClose());
    } else {
      return axios.patch(`http://localhost:8000/api/account/${accountToEdit._id}`, {
          name,
          number,
          shortname,
          desc,
          fgColor,
          bgColor
        })
        .then((res) => onEdit(res.data))
        .then(() => this.props.onClose());
    }
  }

  handleChangeField(key, event) {
    this.setState({
      [key]: event.target.value,
    });
  }

  render() {
    const { accountToEdit } = this.props;
    const { name, number, shortname, desc, fgColor, bgColor, startdate, initbalance } = this.state;

    return (
        <Form size="huge">
          <Form.Group widths='equal'>
            <Form.Input label='Name' onChange={(ev) => this.handleChangeField('name', ev)}
                  value={name} placeholder="Name" type="text" />
            <Form.Input label='Account number' onChange={(ev) => this.handleChangeField('number', ev)}
                  value={number} placeholder="Account number" type="text" />
            <Form.Input label='Short name' onChange={(ev) => this.handleChangeField('shortname', ev)}
                  value={shortname} placeholder="Short name" type="text" />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field
                    control={TextArea}
                    label='Description'
                    value={desc}
                    placeholder='Description'
                    onChange={(ev) => this.handleChangeField('desc', ev)}
                  />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Foreground colour' onChange={(ev) => this.handleChangeField('fgColor', ev)}
                  value={fgColor} placeholder="Foreground colour" type="color" />
            <Form.Input label='Background colour' onChange={(ev) => this.handleChangeField('bgColor', ev)}
                  value={bgColor} placeholder="Background colour" type="color" />
            <Form.Input label='Starting date' onChange={(ev) => this.handleChangeField('startdate', ev)}
                  value={startdate} placeholder="Starting date" type="date" />
            <Form.Input label='Starting balance' onChange={(ev) => this.handleChangeField('initbalance', ev)}
                  value={initbalance} placeholder="Starting balance" type="number" />
          </Form.Group>
        <Button onClick={this.handleSubmit} type='submit'>{accountToEdit ? 'Update' : 'Submit'}</Button>
      </Form>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmit: data => dispatch({ type: 'SUBMIT_ACCOUNT', data }),
  onEdit: data => dispatch({ type: 'EDIT_ACCOUNT', data }),
});

const mapStateToProps = state => ({
  accountToEdit: state.account.accountToEdit,
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountForm);
