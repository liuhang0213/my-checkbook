import React from 'react';
import axios from 'axios';
import moment from 'moment';
import { connect } from 'react-redux';

import {
    Button,
    Container,
    Header,
    Icon,
    Label,
    Menu,
    Modal,
    Table
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

import AccountForm from './AccountForm';


class Account extends React.Component {
  constructor(props) {
      super(props);

      this.state = { formOpen: false }

      this.handleDelete = this.handleDelete.bind(this);
      this.handleEdit = this.handleEdit.bind(this);
      this.handleFormOpen = this.handleFormOpen.bind(this);
      this.handleFormClose = this.handleFormClose.bind(this);
  }

  componentDidMount() {
    const { onLoad } = this.props;

    axios.all([
        axios.get('http://localhost:8000/api/account'),
    ]).then(axios.spread(function (acc) {
        let data = acc.data;
        onLoad(data);
    }));
  }

  handleDelete(id) {
    const { onDelete } = this.props;

    return axios.delete(`http://localhost:8000/api/account/${id}`)
      .then(() => onDelete(id));
  }

  handleEdit(account) {
    const { setEdit } = this.props;
    setEdit(account);
    this.handleFormOpen();
}

  handleFormOpen() {
      this.setState({ formOpen: true })
  }

  handleFormClose() {
      this.setState({ formOpen: false })
  }

  render() {
    const { accounts, accountToEdit } = this.props;
    const testAccount = "test"

    return (
      <Container>
        <p>Current Accounts:</p>
        <Menu attached='top'>
          <Menu.Item>
              <Modal
                  trigger={
                      <Button icon onClick={this.handleFormOpen}>
                          <Icon name="plus" size="big" color='violet' />
                      </Button>}
                  open={this.state.formOpen} onClose={this.handleFormClose}>
              <Modal.Header>
                <Icon name='plus' /> Add New Account
              </Modal.Header>
              <Modal.Content>
                  <Modal.Description>
                      <AccountForm onClose={ this.handleFormClose } />
                  </Modal.Description>
              </Modal.Content>
            </Modal>
          </Menu.Item>
        </Menu>
          <Table celled selectable size='large'>
          <Table.Header>
            <Table.Row inverted>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Number</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Actions</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
          {accounts.map((acc) => {
            return (
                <Table.Row>
                  <Table.Cell><Label horizontal
                      style={{
                          color: acc.fgColor,
                          backgroundColor: acc.bgColor,
                          borderColor: acc.bgColor
                      }}
                      >
                      { acc.shortname }
                    </Label>{ acc.name }</Table.Cell>
                  <Table.Cell>{ acc.number }</Table.Cell>
                  <Table.Cell>{ acc.desc }</Table.Cell>
                  <Table.Cell>
                    <Button icon onClick={() => this.handleEdit(acc)}>
                      <Icon name="edit" color='violet' />
                    </Button>
                    <Button icon onClick={() => this.handleDelete(acc._id)}>
                      <Icon name="delete" color='violet' />
                    </Button>
                  </Table.Cell>
                </Table.Row>
            )})}
          </Table.Body>
          </Table>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  accounts: state.account.accounts,
  accountToEdit: state.account.accountToEdit
});

const mapDispatchToProps = dispatch => ({
  onLoad: data => dispatch({ type: 'ACCOUNT_PAGE_LOADED', data }),
  onDelete: id => dispatch({ type: 'DELETE_ACCOUNT', id }),
  setEdit: accountToEdit => dispatch({ type: 'SET_ACCOUNT_EDIT', accountToEdit }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);
