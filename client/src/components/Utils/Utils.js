export default class Utils {
    static arrWithIdToObject(arr) {
        let re = {};
        arr.forEach((record) => {
            re[record["_id"]] = record
        });
        return re;
    }
}
