import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';

import {
  Container,
} from 'semantic-ui-react'

import { Home, Account, Category } from '../../components';
import { TopMenu } from '../elements';

const App = (props) => {
  return (
    <Container>
      <TopMenu />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/accounts" component={Account} />
        <Route exact path="/categories" component={Category} />
      </Switch>
    </Container>
  )
}

export default withRouter(App);
