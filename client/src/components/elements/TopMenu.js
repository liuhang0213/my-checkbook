import React from 'react'
import {
  Container,
  Divider,
  Dropdown,
  Grid,
  Header,
  Image,
  List,
  Menu,
  Segment,
} from 'semantic-ui-react'

const TopMenu = () => (
    <Menu inverted size="huge">
      <Container>
        <Menu.Item as='a' href='/' header>
          <Image size='mini' src='/assets/img/logo-transparent.png' style={{ marginRight: '1.5em' }} />
          My Checkbook
        </Menu.Item>
        <Menu.Item as='a' href="/accounts">Accounts</Menu.Item>
        <Menu.Item as='a' href="/categories">Categories</Menu.Item>
      </Container>
    </Menu>
)

export default TopMenu
