import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Dropdown,
    Form,
    Input,
    Select,
    TextArea
} from 'semantic-ui-react'

class RecordForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      date: '',
      category: '',
      account: '',
      amount: '',
      notes: '',
      attachmentPath: '',
      confirmed: '',
    }

    this.handleChangeField = this.handleChangeField.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
      if(this.props.recordToEdit) {
          this.setState({
              date: this.props.recordToEdit.date,
              category: this.props.recordToEdit.category,
              account: this.props.recordToEdit.account,
              amount: this.props.recordToEdit.amount,
              notes: this.props.recordToEdit.notes,
              attachmentPath: this.props.recordToEdit.attachmentPath,
              confirmed: this.props.recordToEdit.confirmed
          });
      }
  }

  handleSubmit(){
    const { onSubmit, recordToEdit, onEdit } = this.props;
    const { date,
          category,
          account,
          amount,
          notes,
          attachmentPath,
          confirmed } = this.state;

    if(!recordToEdit) {
      return axios.post('http://localhost:8000/api/record', {
          date,
          category,
          account,
          amount,
          notes,
          attachmentPath,
          confirmed
      })
        .then((res) => onSubmit(res.data))
        .then(() => this.props.onClose());
    } else {
      return axios.patch(`http://localhost:8000/api/record/${recordToEdit._id}`, {
          date,
          category,
          account,
          amount,
          notes,
          attachmentPath,
          confirmed
      })
        .then((res) => onEdit(res.data))
        .then(() => this.props.onClose());
    }
  }

  handleChangeField(key, event) {
    this.setState({
      [key]: event.target.value,
    });
  }

  handleChangeSelect(key, data) {
    this.setState({
      [key]: data.value,
    });
  }

  render() {
    const { recordToEdit } = this.props;
    const { accounts, categories } = this.props;
    const {
          date,
          category,
          account,
          amount,
          notes,
          attachmentPath,
          confirmed,
    } = this.state;

    const categoryOptions = categories.map((cat) =>
        ({ key: cat._id, value: cat._id, text: cat.name })
        //<option value={ cat._id } selected>{ cat.name }</option>
    );

    const accountOptions = accounts.map((acc) =>
        ({ key: acc._id, value: acc._id, text: acc.name })
    );

    return (
        <Form size="huge">
          <Form.Group widths='equal'>
            <Form.Input label='Date' onChange={(ev) => this.handleChangeField('date', ev)}
                value={date} placeholder="Date" type='date' />
            <Form.Input label='Amount' onChange={(ev) => this.handleChangeField('amount', ev)}
                value={amount} placeholder="Amount" type="number" />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field>
              <label>Category</label>
              <Form.Select onChange={(ev, data) => this.handleChangeSelect('category', data)}
                  value={category} options={categoryOptions} />
            </Form.Field>
            <Form.Field>
              <label>Account</label>
              <Form.Select onChange={(ev, data) => this.handleChangeSelect('account', data)}
                  value={account} options={accountOptions} />
            </Form.Field>
          </Form.Group>

        <Form.Field
          control={TextArea}
          label='Notes'
          value={notes}
          placeholder='Notes'
          onChange={(ev) => this.handleChangeField('notes', ev)}
        />
        <Form.Input
          label='Attachment'
          onChange={(ev) => this.handleChangeField('attachmentPath', ev)}
          value={attachmentPath}
          placeholder="Attachment"
          type='text'
        />
        <Button onClick={this.handleSubmit} type='submit'>{recordToEdit ? 'Update' : 'Submit'}</Button>
      </Form>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmit: data => dispatch({ type: 'SUBMIT_RECORD', data }),
  onEdit: data => dispatch({ type: 'EDIT_RECORD', data }),
});

const mapStateToProps = state => ({
  recordToEdit: state.home.recordToEdit,
});

export default connect(mapStateToProps, mapDispatchToProps)(RecordForm);
