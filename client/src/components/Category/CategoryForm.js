import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Dropdown,
    Form,
    Input,
    Select,
    TextArea
} from 'semantic-ui-react'

class CategoryForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      fgColor: '',
      type: '',
    }

    this.handleChangeField = this.handleChangeField.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if(this.props.categoryToEdit) {
      this.setState({
        name: this.props.categoryToEdit.name,
        fgColor: this.props.categoryToEdit.fgColor,
        type: this.props.categoryToEdit.type,
      });
    }
  }

  handleSubmit(){
    const { onSubmit, categoryToEdit, onEdit } = this.props;
    const { name, fgColor, type } = this.state;

    if(!categoryToEdit) {
      return axios.post('http://localhost:8000/api/category', {
        name,
        fgColor,
        type,
      })
        .then((res) => onSubmit(res.data))
        .then(() => this.props.onClose());
    } else {
      return axios.patch(`http://localhost:8000/api/category/${categoryToEdit._id}`, {
        name,
        fgColor,
        type,
      })
        .then((res) => onEdit(res.data))
        .then(() => this.props.onClose());
    }
  }

  handleChangeField(key, event) {
    this.setState({
      [key]: event.target.value,
    });
  }

  handleChangeSelect(key, data) {
    this.setState({
      [key]: data.value,
    });
  }

  render() {
    const { categoryToEdit } = this.props;
    const { name, fgColor, type } = this.state;
    const typeOptions = [
        { key: 1, value: "Income", text: "Income" },
        { key: 2, value: "Expense", text: "Expense" },
    ]

    return (
        <Form size="huge">
          <Form.Group widths='equal'>
            <Form.Input label='Name' onChange={(ev) => this.handleChangeField('name', ev)}
                  value={name} placeholder="Name" type="text" />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input label='Colour' onChange={(ev) => this.handleChangeField('fgColor', ev)}
                  value={fgColor} placeholder="Foreground colour" type="color" />
            <Form.Select onChange={(ev, data) => this.handleChangeSelect('type', data)}
                  value={type} options={typeOptions} />
          </Form.Group>]
        <Button onClick={this.handleSubmit} type='submit'>{categoryToEdit ? 'Update' : 'Submit'}</Button>
      </Form>
    )
  }
}

const mapDispatchToProps = dispatch => ({
  onSubmit: data => dispatch({ type: 'SUBMIT_CATEGORY', data }),
  onEdit: data => dispatch({ type: 'EDIT_CATEGORY', data }),
});

const mapStateToProps = state => ({
  categoryToEdit: state.category.categoryToEdit,
});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryForm);
