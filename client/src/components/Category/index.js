import React from 'react';
import axios from 'axios';
import moment from 'moment';
import { connect } from 'react-redux';

import {
    Button,
    Container,
    Header,
    Icon,
    Label,
    Menu,
    Modal,
    Table
} from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';

import CategoryForm from './CategoryForm';

class Category extends React.Component {
  constructor(props) {
      super(props);

      this.state = { formOpen: false }

      this.handleDelete = this.handleDelete.bind(this);
      this.handleEdit = this.handleEdit.bind(this);
      this.handleFormOpen = this.handleFormOpen.bind(this);
      this.handleFormClose = this.handleFormClose.bind(this);
  }

  componentDidMount() {
    const { onLoad } = this.props;

    axios.all([
        axios.get('http://localhost:8000/api/category'),
    ]).then(axios.spread(function (cat) {
        let data = cat.data;
        onLoad(data);
    }));
  }

  handleDelete(id) {
    const { onDelete } = this.props;

    return axios.delete(`http://localhost:8000/api/category/${id}`)
      .then(() => onDelete(id));
  }

  handleEdit(category) {
    const { setEdit } = this.props;

    setEdit(category);
    this.handleFormOpen();
  }

  handleFormOpen() {
      this.setState({ formOpen: true })
  }

  handleFormClose() {
      this.setState({ formOpen: false })
  }

  render() {
    const { categories, categoryToEdit } = this.props;

    return (
      <Container>
        <p>Current Categories:</p>
        <Menu attached='top'>
          <Menu.Item>
              <Modal
                  trigger={
                      <Button icon onClick={this.handleFormOpen}>
                          <Icon name="plus" size="big" color='violet' />
                      </Button>}
                  open={this.state.formOpen} onClose={this.handleFormClose}>
              <Modal.Header>
                <Icon name='plus' /> Add New Category
              </Modal.Header>
              <Modal.Content>
                <Modal.Description>
                    <CategoryForm categoryToEdit={ categoryToEdit }
                        onClose={ this.handleFormClose }/>
                </Modal.Description>
              </Modal.Content>
            </Modal>
          </Menu.Item>
        </Menu>
          <Table celled selectable size='large'>
          <Table.Header>
            <Table.Row inverted>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Type</Table.HeaderCell>
              <Table.HeaderCell>Actions</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
          {categories.map((cat) => {
            return (
                <Table.Row>
                  <Table.Cell><Label horizontal basic
                      style={{
                          color: cat.fgColor,
                      }}
                      >
                      { cat.name }
                    </Label></Table.Cell>
                  <Table.Cell>{ cat.type }</Table.Cell>
                  <Table.Cell>
                    <Button icon onClick={() => this.handleEdit(cat)}>
                      <Icon name="edit" color='violet' />
                    </Button>
                    <Button icon onClick={() => this.handleDelete(cat._id)}>
                      <Icon name="delete" color='violet' />
                    </Button>
                  </Table.Cell>
                </Table.Row>
            )})}
          </Table.Body>
          </Table>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.category.categories,
  categoryToEdit: state.category.categoryToEdit
});

const mapDispatchToProps = dispatch => ({
  onLoad: data => dispatch({ type: 'CATEGORY_PAGE_LOADED', data }),
  onDelete: id => dispatch({ type: 'DELETE_CATEGORY', id }),
  setEdit: category => dispatch({ type: 'SET_EDIT_CATEGORY', category }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Category);
