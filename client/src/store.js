import { createStore, combineReducers } from 'redux';

import { home, account, category } from './reducers';

const reducers = combineReducers({
  home, account, category
});

const store = createStore(reducers);

export default store;
