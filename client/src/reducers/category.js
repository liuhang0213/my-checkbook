export default (state={categories: []}, action) => {
  switch(action.type) {
    case 'CATEGORY_PAGE_LOADED':
      return {
        ...state,
        categories: action.data.category,
      };
    case 'DELETE_CATEGORY':
      return {
        ...state,
        categories: state.categories.filter((category) => category._id !== action.id),
      };
    case 'SET_EDIT_CATEGORY':
      return {
        ...state,
        categoryToEdit: action.category,
      };
      case 'EDIT_CATEGORY':
        return {
          ...state,
          categories: state.categories.map((category) => {
            if(category._id === action.data.category._id) {
              return {
                ...action.data.category,
              }
            }
            return category;
          }),
          categoryToEdit: undefined,
        }
    default:
      return state;
  }
};
