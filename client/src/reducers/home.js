export default (state={records: [], accounts: [], categories: []}, action) => {
  switch(action.type) {
    case 'HOME_PAGE_LOADED':
      return {
        ...state,
        records: action.data.record,
        accounts: action.data.account,
        categories: action.data.category,
      };
    case 'SUBMIT_RECORD':
      return {
        ...state,
        records: ([action.data.record]).concat(state.records),
      };
    case 'SUBMIT_CATEGORY':
      return {
        ...state,
        records: ([action.data.record]).concat(state.records),
      };
    case 'DELETE_RECORD':
      return {
        ...state,
        records: state.records.filter((record) => record._id !== action.id),
      };
    case 'SET_EDIT':
      return {
        ...state,
        recordToEdit: action.record,
      };
    case 'EDIT_RECORD':
      return {
        ...state,
        records: state.records.map((record) => {
          if(record._id === action.data.record._id) {
            return {
              ...action.data.record,
            }
          }
          return record;
        }),
        recordToEdit: undefined,
      }
    default:
      return state;
  }
};
