export default (state={accounts: []}, action) => {
  switch(action.type) {
    case 'ACCOUNT_PAGE_LOADED':
      return {
        ...state,
        accounts: action.data.account,
      };
    case 'DELETE_ACCOUNT':
      return {
        ...state,
        accounts: state.accounts.filter((account) => account._id !== action.id),
      };
    case 'SET_ACCOUNT_EDIT':
      return {
        ...state,
        accountToEdit: action.accountToEdit,
      };
    case 'EDIT_ACCOUNT':
        return {
          ...state,
          accounts: state.accounts.map((account) => {
            if(account._id === action.data.account._id) {
              return {
                ...action.data.account,
              }
            }
            return account;
          }),
          accountToEdit: undefined,
        }
    default:
      return state;
  }
};
